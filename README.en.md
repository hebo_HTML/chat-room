# 聊天室

#### Description
（九）聊天室小程序或QQ(3-4人) 等级：A
功能要求：
聊天室：使用图形用户界面，能实现一个聊天室中多人聊天，可以两人私聊，可以发送文件。
QQ:实现类似QQ用户注册、登录、聊天等功能。
注意：有一定等级。完全照搬别人的代码，不超过70分。
提示：使用socket通信

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
