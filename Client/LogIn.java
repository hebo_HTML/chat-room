package Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LogIn extends JFrame implements ActionListener {
    //三个面板
    private JPanel pImg;
    private JPanel pSR;
    private JPanel pDZ;

    private Box baseBox;
    private Box boxV1;
    private Box boxV2;
    private Box boxV3;

    private JTextField ID=new JTextField(20);
    private JTextField Password=new JTextField(20);

    private JButton Deng;
    private JButton Zhu;

    private JLabel Img;

    public LogIn(){
        Img=new JLabel();
        Img.setIcon(new ImageIcon("F:\\Test\\chat-room\\Client\\R-C.jpg"));
        pImg=new JPanel();
        pImg.add(Img);
        pImg.setPreferredSize(new Dimension(500,150));
        add(pImg);

        boxV1=Box.createVerticalBox();//创建垂直框
        boxV1.add(new JLabel("账号："));//创建标签
        boxV1.add(Box.createVerticalStrut(8));//记录间隔
        boxV1.add(new JLabel("密码："));
        boxV2=Box.createVerticalBox();//创建垂直框
        boxV2.add(ID);
        boxV2.add(Box.createVerticalStrut(8));
        boxV2.add(Password);
        baseBox=Box.createHorizontalBox();
        baseBox.add(boxV1);
        baseBox.add(Box.createHorizontalStrut(10));
        baseBox.add(boxV2);
        pSR=new JPanel();
        pSR.add(baseBox);

        Deng=new JButton("登录");
        Zhu=new JButton("注册");

        boxV3=Box.createHorizontalBox();
        boxV3.add(Deng);
        boxV3.add(Box.createHorizontalStrut(10));
        boxV3.add(Zhu);
        pDZ=new JPanel();
        pDZ.add(boxV3);

        add(pImg,BorderLayout.NORTH);
        add(pSR,BorderLayout.CENTER);
        add(pDZ,BorderLayout.SOUTH);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setBounds(250,250,425,320);
        validate();
    }
    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
