package Client;

//import chat3.PersonelView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HomeJframe extends JFrame {
    //顶部
    JPanel jptop = new JPanel();
    //置于中间来存放好友列表
    JPanel jcenter = new JPanel();
    //顶部软件名
    JLabel jltitle = new JLabel("he_zhang_yang_chat");
    //头像
    JLabel jtouxiang = new JLabel(new ImageIcon("D:\\java\\work\\src\\chat3\\personelView1.jpg"));
    //昵称
    JLabel jMyname = new JLabel("who");

    JTextField JFind = new JTextField("请输入账号查找联系人");
    //查找按钮并用一个图形来表示
    ImageIcon imageFind = new ImageIcon("D:\\java\\work\\src\\chat3\\personelViewFind.jpg");
    JButton jBFind = new JButton(imageFind); //查找联系人按钮
    //过渡顶部和中部好友面板
    JLabel jcenter1 = new JLabel(new ImageIcon("D:\\java\\work\\src\\chat3\\personelViewCenter1.jpg"));//过渡
    //中部
    JTabbedPane jChoose = new JTabbedPane(); //选项板
    JPanel jpcenterFriend = new JPanel();   //置于中间的中间存放好友列表

    JLabel j1 = new JLabel("nicai");
    JLabel j2 = new JLabel("nicai2");

    DefaultListModel listModel=new DefaultListModel();
    JList userList=new JList(listModel);
    JScrollPane jSuserList=new JScrollPane(userList);
    //增加用户的标签”分组外“的弹出式菜单
    JMenuItem jM11=new JMenuItem("列表显示");
    JMenuItem jM12=new JMenuItem("刷新好友列表");
    JMenuItem jM13=new JMenuItem("显示在线联系人");
    JMenuItem jM14=new JMenuItem("添加联系人");
    JMenuItem jM15=new JMenuItem("查找好友");
    JMenuItem jM16=new JMenuItem("好友管理器");
    JMenuItem jM17=new JMenuItem("帮助");
    JMenuItem jM18=new JMenuItem("关于");
    JPopupMenu jPmenuser=new JPopupMenu();

    JLabel jbase = new JLabel(new ImageIcon("D:\\java\\work\\src\\chat3\\personelViewCenter1.jpg"));

    public void init(){
        //顶部面板
        jptop.setBounds(0,0,313,110);
        jptop.setLayout(null);
        jptop.setBackground(Color.PINK);

        jltitle.setForeground(Color.white);
        jltitle.setFont(new Font("黑体",Font.BOLD,15));
        jltitle.setBounds(0,0,313,25);

        jtouxiang.setBounds(5, 25, 60, 55);

        jMyname.setForeground(Color.WHITE);
        jMyname.setFont(new Font("宋体",Font.BOLD,17));
        jMyname.setBounds(80,27,180,25);

        JMenuItem jMa = new JMenuItem("修改个人资料");
        jMa.setFont(new Font("楷体",Font.PLAIN,14));
        jMa.setForeground(Color.BLACK);
        jMa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        JPopupMenu jPopupMenu = new JPopupMenu();
        jPopupMenu.add(jMa);

        jMyname.setComponentPopupMenu(jPopupMenu);

        JFind.setFont(new Font("宋体",Font.BOLD,12));
        JFind.setForeground(Color.GRAY);
        JFind.setBounds(3,85,260,25);

        jBFind.setBounds(267,87,19,21);
        jBFind.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        jptop.add(jBFind);
        jptop.add(JFind);
        jptop.add(jMyname);
        jptop.add(jtouxiang);
        jptop.add(jltitle);
//        jptop.add(jltitle);

        jcenter1.setBounds(0,110,313,7);

//        jpcenterFriend.add(jM11);

        userList.setComponentPopupMenu(jPmenuser);

        jChoose.setForeground(Color.DARK_GRAY);
        jChoose.setBackground(Color.WHITE);
        jChoose.setFont(new Font("宋体",Font.PLAIN,11));
        jChoose.addTab("联系人",jcenter);
//        jChoose.addTab("群",j1);
        jChoose.setBounds(0,117,313,382);

        jcenter.setLayout(new BorderLayout());
//        jcenter.add(jChoose);

        JLabel test = new JLabel("我的好友");
        test.setFont(new Font("宋体",Font.PLAIN,14));
        test.setSize(313,30);
        test.setForeground(Color.BLACK);

        jPmenuser.add(jM11);
        jPmenuser.add(jM12);
        jPmenuser.add(jM13);
        jPmenuser.add(jM14);
        jPmenuser.add(jM15);
        jPmenuser.add(jM16);
        jPmenuser.add(jM17);
        jPmenuser.add(jM18);
        jM11.setFont(new Font("楷体",Font.PLAIN,14));
        jM11.setForeground(Color.DARK_GRAY);
        jM12.setFont(new Font("楷体",Font.PLAIN,14));
        jM12.setForeground(Color.DARK_GRAY);
        jM13.setFont(new Font("楷体",Font.PLAIN,14));
        jM13.setForeground(Color.DARK_GRAY);
        jM14.setFont(new Font("楷体",Font.PLAIN,14));
        jM14.setForeground(Color.DARK_GRAY);
        jM14.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        jM15.setFont(new Font("楷体",Font.PLAIN,14));
        jM15.setForeground(Color.DARK_GRAY);
        jM15.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        jM16.setFont(new Font("楷体",Font.PLAIN,14));
        jM16.setForeground(Color.DARK_GRAY);
        jM17.setFont(new Font("楷体",Font.PLAIN,14));
        jM17.setForeground(Color.DARK_GRAY);
        jM18.setFont(new Font("楷体",Font.PLAIN,14));
        jM18.setForeground(Color.DARK_GRAY);
        jM18.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        test.setComponentPopupMenu(jPmenuser);
        jcenter.setBackground(Color.WHITE);
        jcenter.setBounds(0,117,313,382);
//        userList.addMouseListener(new PersonelView.PersonelView_userList_mouseMotionAdapter());
        jcenter.add(BorderLayout.NORTH,test);
        jcenter.add(jSuserList);
//        jcenter.add(jChoose);

        jbase.setBounds(0,499,304,51);
        this.setSize(313, 590);
        this.setLocationRelativeTo(null);
        this.setLayout(null);
        this.add(jptop);
        this.add(jcenter1);
        this.add(jcenter);
        this.add(jbase);
        this.add(jChoose);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setBounds(0,0,310,500);
        validate();
    }
    public HomeJframe() {
        init();
    }

    public static void main(String[] args) {
        new HomeJframe();
    }
}
