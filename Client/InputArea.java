package Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class InputArea extends JPanel implements ActionListener {
    File f=null;//创建文件
    RandomAccessFile out;//动态读取文件内容
    Box baseBox,boxV1,boxV2;//创建box容器
    JTextField name,email,phone;//创建文本框
    JButton button;//创建按钮
    InputArea(File f){
        setBackground(Color.cyan);//设置页面背景
        this.f=f;//获取文件地址
        name=new JTextField(12);//设置文本框大小
        email=new JTextField(12);
        phone=new JTextField(12);
        button=new JButton("录入");//设置按钮名称
        boxV1=Box.createVerticalBox();//创建文件垂直框
        boxV1.add(new JLabel("输入姓名"));//创建标签名称
        boxV1.add(Box.createVerticalStrut(8));//创建垂直支柱
        boxV1.add(new JLabel("输入email"));
        boxV1.add(Box.createVerticalStrut(8));
        boxV1.add(new JLabel("输入电话"));
        boxV1.add(Box.createVerticalStrut(8));
        boxV1.add(new JLabel("单击录入"));
        boxV2=Box.createVerticalBox();//创建垂直框
        boxV2.add(name);
        boxV2.add(Box.createVerticalStrut(8));
        boxV2.add(email);
        boxV2.add(Box.createVerticalStrut(8));
        boxV2.add(phone);
        boxV2.add(Box.createVerticalStrut(8));
        boxV2.add(button);
        baseBox=Box.createHorizontalBox();
        baseBox.add(boxV1);
        baseBox.add(Box.createHorizontalStrut(10));
        baseBox.add(boxV2);
        add(baseBox);
        button.addActionListener(this);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            RandomAccessFile out=new RandomAccessFile(f,"rw");
            if (f.exists()){
                long length=f.length();
                out.seek(length);
            }
            out.writeUTF("姓名："+name.getText());
            out.writeUTF("email:"+email.getText());
            out.writeUTF("电话："+phone.getText());
            out.close();
        }
        catch (IOException e1){
        }
    }
}