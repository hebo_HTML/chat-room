package Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

final class Register1 extends JFrame implements ActionListener {
    private Box baseBox;
    private Box boxV1;
    private Box boxV2;
    private Box boxV3;

    JTextField Name=new JTextField(20);
    JLabel alertName=new JLabel("请输入昵称");

    JTextField Password=new JTextField(20);
    JLabel alertPass=new JLabel("长度为6-16个字符，不能包含空格");

    JPasswordField jRepass=new JPasswordField();
    JLabel alertRePass=new JLabel("请再次输入密码");

    JTextField jtsex=new JTextField(10);
    JLabel alertsex = new JLabel("请填“男”或“女”");

//    ButtonGroup radioGroup=new ButtonGroup();
//    JRadioButton  boy=new JRadioButton("男",false);
//    JRadioButton  girl=new JRadioButton("女",false);

    JTextField Birthday=new JTextField(20);
    JLabel alertbirthday = new JLabel("格式为：xxxx/xx/xx");

    JTextField Phone=new JTextField(20);
    JLabel alertphone = new JLabel("请输入你的手机号");

    JButton zc_button = new JButton("立即注册");
    JPanel panel=new JPanel();
    JLabel backGround = new JLabel(new ImageIcon("D:\\java\\chat-room\\Client\\1.jpg"));
    public Register1(){
        super("注册界面");
        boxV1=Box.createVerticalBox();//创建垂直框
        boxV1.add(Box.createVerticalStrut(6));
        boxV1.add(new JLabel("昵称："));

        boxV1.add(Box.createVerticalStrut(16));
        boxV1.add(new JLabel("密码："));

        boxV1.add(Box.createVerticalStrut(16));
        boxV1.add(new JLabel("确认密码："));

        boxV1.add(Box.createVerticalStrut(15));
        boxV1.add(new JLabel("性别："));

        boxV1.add(Box.createVerticalStrut(15));
        boxV1.add(new JLabel("出生年月："));

        boxV1.add(Box.createVerticalStrut(16));
        boxV1.add(new JLabel("电话号码："));

        boxV2=Box.createVerticalBox();
        boxV2.add(Box.createVerticalStrut(8));
        boxV2.add(Name);
        boxV2.add(Box.createVerticalStrut(8));
        boxV2.add(Password);
        boxV2.add(Box.createVerticalStrut(8));
        boxV2.add(jRepass);
        boxV2.add(Box.createVerticalStrut(8));
        boxV2.add(jtsex);
        boxV2.add(Box.createVerticalStrut(8));
        boxV2.add(Birthday);
        boxV2.add(Box.createVerticalStrut(8));
        boxV2.add(Phone);

        boxV3=Box.createVerticalBox();
        boxV3.add(Box.createVerticalStrut(6));
        boxV3.add(alertName);
        boxV3.add(Box.createVerticalStrut(16));
        boxV3.add(alertPass);
        boxV3.add(Box.createVerticalStrut(16));
        boxV3.add(alertRePass);
        boxV3.add(Box.createVerticalStrut(15));
        boxV3.add(alertsex);
        boxV3.add(Box.createVerticalStrut(15));
        boxV3.add(alertbirthday);
        boxV3.add(Box.createVerticalStrut(16));
        boxV3.add(alertphone);

        baseBox=Box.createHorizontalBox();
        baseBox.add(Box.createHorizontalStrut(0));
        baseBox.add(boxV1);
        baseBox.add(Box.createHorizontalStrut(0));
        baseBox.add(boxV2);
        baseBox.add(Box.createHorizontalStrut(0));
        baseBox.add(boxV3);

        panel.add(baseBox,BorderLayout.CENTER);
        add(panel, BorderLayout.NORTH);
//        backGround.setForeground(Color.DARK_GRAY);
//        backGround.setBounds(100,250,500,320);
        add(backGround);
        zc_button.setBounds(1,1,1,1);
        add(zc_button,BorderLayout.SOUTH);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setBounds(250,250,500,320);
        validate();
    }
    @Override
    public void actionPerformed(ActionEvent e) {

    }

    public static void main(String[] args) {
        new Register1();
    }
}
