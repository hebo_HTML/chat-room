package Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.RandomAccessFile;

public class CommFrame extends JFrame implements ActionListener {
    File file=null;//创建文件
    JMenuBar bar;//创建菜单条
    JMenu fileMenu;//创建菜单
    JMenuItem inputMenuItem,showMenuItem;//创建菜单项
    JTextArea show;//负责显示信息
    InputArea inputMessage;//负责录入信息（InputArea是自己写的类）
    CardLayout card=null;//卡片式布局
    JPanel pCenter;//创建面板
    CommFrame(){
        file=new File("F:\\java\\java练习\\第十章\\通讯录.txt");//指定文件地址
        inputMenuItem=new JMenuItem("录入");//创建菜单项
        showMenuItem=new JMenuItem("显示");
        bar=new JMenuBar();//创建菜单条
        fileMenu=new JMenu("菜单选项");//创建菜单
        fileMenu.add(inputMenuItem);//将菜单项加入菜单
        fileMenu.add(showMenuItem);
        bar.add(fileMenu);//将菜单加入菜单条
        setJMenuBar(bar);//设置菜单条
        inputMenuItem.addActionListener(this);
        showMenuItem.addActionListener(this);
        inputMessage=new InputArea(file);//将文件地址写入插入文件模块
        show=new JTextArea(12,20);//创建文本区
        card=new CardLayout();//创建卡片式布局
        pCenter=new JPanel();//创建面板
        pCenter.setLayout(card);//设置布局
        pCenter.add("inputMenuItem",inputMessage);//将面板中插入显示和录入界面
        pCenter.add("showMenuItem",show);
        add(pCenter,BorderLayout.CENTER);//将面板插入到分布式布局的中心
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setBounds(100,50,420,380);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() ==inputMenuItem){
            card.show(pCenter,"inputMenuItem");
        }
        else if (e.getSource()==showMenuItem){
            int number=1;
            show.setText(null);
            card.show(pCenter,"showMenuItem");
            try {
                RandomAccessFile in=new RandomAccessFile(file,"r");
                String name;
                while ((name=in.readUTF())!=null){
                    show.append("\n"+number+" "+name);
                    show.append("\t"+in.readUTF());//读取email
                    show.append("\t"+in.readUTF());//读取phone
                    show.append("\n------------------------- ");
                    number++;
                }
                in.close();
            }
            catch (Exception e1){}
        }
    }
}
