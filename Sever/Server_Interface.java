package Sever;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Server_Interface extends JFrame implements ActionListener, ListSelectionListener {
    JTextArea UserLog=new JTextArea();
    JButton button1=new JButton("查看消息"),
            button2=new JButton("踢出"),
            button3=new JButton("暂停服务"),
            button4=new JButton("退出");
    JLabel ShowList=new JLabel("在线用户列表<10秒刷新一次>");
    JLabel ShowServerLog=new JLabel("服务器日志");
    JLabel UserCount=new JLabel("在线人数 :");
    JLabel Count=new JLabel("0");
    JLabel Time;
    DefaultListModel ListModel=new DefaultListModel();
    JList UserList=new JList(ListModel);
    JScrollPane JSUerList=new JScrollPane(UserList);
    JScrollPane JSUserLog=new JScrollPane(UserList);
    SimpleDateFormat m=new SimpleDateFormat("hh:mm:ss");//设置时间的格式;//创建时间格式
    Server_Interface(){
        setSize(800,700);
        setLocationRelativeTo(null);
        setLayout(null);
        setTitle("服务器控制界面");
        add(ShowList);
        add(JSUerList);
        add(button1);
        add(button2);
        add(UserCount);
        add(Count);
        add(ShowServerLog);
        add(JSUserLog);
        add(button3);
        add(button4);
        ShowList.setBounds(20,10,150,25);
        ShowList.setFont(new Font("宋体",Font.PLAIN,11));
        JSUerList.setBounds(10,40,190,500);
        button1.setBounds(20,550,75,25);
        button1.setFont(new Font("宋体",Font.PLAIN,10));
        button2.setBounds(110, 550, 60, 25);
        button2.setFont(new Font("宋体",Font.PLAIN,10));
        UserCount.setBounds(25, 595, 100, 30);
        UserCount.setFont(new Font("宋体",Font.PLAIN,12));
        Count.setBounds(125, 595, 20, 30);
        Count.setFont(new Font("宋体",Font.PLAIN,12));
        ShowServerLog.setBounds(220, 10, 150, 25);
        ShowServerLog.setFont(new Font("宋体",Font.PLAIN,11));
        JSUserLog.setBounds(210, 40, 575, 550);
        button3.setBounds(340, 595, 90, 25);
        button3.setFont(new Font("宋体",Font.PLAIN,11));
        button4.setBounds(470, 595, 60, 25);
        button4.setFont(new Font("宋体",Font.PLAIN,11));
        Date date=new Date();
        Time=new JLabel("时间："+m.format(date));
        add(Time);
        Time.setBounds(600, 615, 250, 50);
        Time.setFont(new Font("宋体",Font.PLAIN,12));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        validate();
    }
    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
