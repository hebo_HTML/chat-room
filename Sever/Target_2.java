package Sever;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Target_2 extends Thread{//注册界面的操作
    Socket socket;
    DataOutputStream out=null;
    DataInputStream in=null;
    PreparedStatement sqlOne,sqlTwo;//创建预处理语句
    Target_2(Socket t,PreparedStatement sqlOne,PreparedStatement sqlTwo){
        socket=t;
        this.sqlOne=sqlOne;
        this.sqlTwo=sqlTwo;
        try {
            out=new DataOutputStream(socket.getOutputStream());//获取套接字的数据输出流
            in=new DataInputStream(socket.getInputStream());//获取套接字的数据输入流
        }
        catch (IOException e){
            System.out.println(e);
        }
    }
    public void run(){
        while (true){
            try {
                String str=in.readUTF();
                if (str.startsWith("用户信息表:")){
                    str=str.substring(str.indexOf(":"));
                    sqlOne.setString(1,str);
                    if (sqlOne.executeUpdate()>=1){
                        out.writeUTF("注册成功！");
                    }
                    else {
                        out.writeUTF("注册失败！");
                    }
                }
                else if (str.startsWith("信息交流表:")){
                    str=str.substring(str.indexOf(":"));
                    sqlTwo.setString(1,str);
                    if (sqlTwo.executeUpdate()>=1){
                        out.writeUTF("插入成功！");
                    }
                    else {
                        out.writeUTF("插入失败！");
                    }
                }
            }
            catch (IOException e) {
                System.out.println("客户离开"+e);
            }
            catch (SQLException e){
                System.out.println(e);
            }
        }
    }
}
