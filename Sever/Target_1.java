package Sever;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class  Target_1 extends Thread{//主要用于登录和查询联系人
    Socket socket;
    DataOutputStream out=null;//创建数据输出流
    DataInputStream in=null;//创建数据输入流
    PreparedStatement sqlOne,sqlTwo,sqlThree;//创建预处理语句,sqlOne做查询操作，sqlTwo做其他的操作
    boolean boo=false;
    String str;
    Target_1(Socket t, PreparedStatement sqlOne, PreparedStatement sqlTwo,PreparedStatement sqlThree,String str){
        socket=t;
        this.sqlOne=sqlOne;
        this.sqlTwo=sqlTwo;
        this.sqlThree=sqlThree;
        this.str=str;
        try {
            out=new DataOutputStream(socket.getOutputStream());//获取套接字的数据输出流
            in=new DataInputStream(socket.getInputStream());//获取套接字的数据输入流
        }
        catch (IOException e){
            System.out.println(e);
        }
    }
    public void run(){
        String string = null;
        ResultSet rs=null;//创建结果集
        while (true){//主要做查询操作
            try {
//                String str=in.readUTF();
                String str="账号:123";
                if (str.startsWith("账号:")){//检查字符串是否是以指定字符串开头
                    str=str.substring(str.indexOf(":")+1);//返回字符串的子字符串
                    sqlOne.setString(1,str);//setString(1,str)账号=?中的问号，而？账号字段中的任意值
                    rs=sqlOne.executeQuery();//返回查询结果
                    while (rs.next()){
                        boo=true;
                        String id=rs.getString(1);//获取查询结果
                        String name=rs.getString(2);
                        String password=rs.getString(3);
                        Date birthday=rs.getDate(4);
                        String phone=rs.getString(5);
                        string="账号："+id+";昵称："+name+";密码："+password+
                                ";生日："+birthday+";手机号："+phone+";";
                        out.writeUTF("账号："+id+";昵称："+name+";密码："+password+
                                ";生日："+birthday+";手机号："+phone+";");
                    }
                    if (!boo){
                        string="查无此人！";
                        out.writeUTF("查无此人！");
                    }
                }
                else if (str.startsWith("昵称:")){//检查字符串是否是以指定字符串开头
                    str=str.substring(str.indexOf(":")+1);//返回字符串的子字符串
                    sqlTwo.setString(1,str);
                    rs=sqlTwo.executeQuery();//返回查询结果
                    while (rs.next()){
                        boo=true;
                        String id=rs.getString(1);//获取查询结果
                        String name=rs.getString(2);
                        String password=rs.getString(3);
                        Date birthday=rs.getDate(4);
                        String phone=rs.getString(5);
                        string="账号："+id+";昵称："+name+";密码："+password+
                                ";生日："+birthday+";手机号："+phone+";";
                        out.writeUTF("账号："+id+";昵称："+name+";密码："+password+
                                ";生日："+birthday+";手机号："+phone+";");
                    }
                    if (!boo){
                        string="查无此人！";
                        out.writeUTF("查无此人！");
                    }
                }
                else if (str.startsWith("账号1:")){
                    String s1=str.substring(str.indexOf(":")+1,str.indexOf(" "));
                    String s2=str.substring(str.indexOf("：")+1);
                    sqlThree.setString(1,s1);
                    sqlThree.setString(2,s2);
                    rs=sqlThree.executeQuery();//返回查询结果
                    while (rs.next()){
                        boo=true;
                        String id1=rs.getString(1);
                        String id2=rs.getString(2);
                        String text=rs.getString(3);
                        string="账号1："+id1+";账号2："+id2+";"+
                                "消息："+text+";";
                        out.writeUTF("账号1："+id1+";账号2："+id2+";"+
                                "消息："+text+";");
                    }
                    if (!boo){
                        string="查无此人！";
                        out.writeUTF("查无此人！");
                    }
                }
                str=string;
            }
            catch (IOException e){
                System.out.println("客户离开"+e);
                return;
            }
            catch (SQLException e){
                System.out.println(e);
            }
        }
    }
}
