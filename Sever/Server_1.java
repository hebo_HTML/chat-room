package Sever;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;

public class Server_1 {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }//加载驱动程序  5.5以前的版本需要加载驱动器
        Connection conn;
        Server_Interface server_interface=new Server_Interface();
        ServerSocket server=null;
        ServerThread thread=null;
        Socket client=null;
        String uri =
                "jdbc:mysql://localhost:3306/chat-room?user=root&password=hebo20010210&useSSL=true&characterEncoding=utf-8";//链接字符串
        try {
            conn = DriverManager.getConnection(uri); //连接代码||跟数据库建立连接，并创建连接对象
            if (conn != null) {
                server_interface.UserLog.setText("数据库连接成功！");
                server_interface.UserLog.append("\n正在等待用户呼叫...");
                Statement stm = conn.createStatement();//创建了一个sql语句对象，相当于发送器
                while (true){
                    try {
                        server=new ServerSocket(2010);//进行端口连接
                    }
                    catch (IOException e){
                        server_interface.UserLog.append("\n正在监听："+client.getInetAddress());
                    }
                    try {
                        server_interface.UserLog.append("\n等待客户呼叫");
                        client=server.accept();
                    }
                    catch (IOException e){
                        server_interface.UserLog.append("\n正在等待客户");
                    }
                    if (client!=null){
                        new ServerThread(client).start();
                    }
                    conn.close();
                }
            }
        } catch (SQLException ignored) {
            server_interface.UserLog.setText("数据库连接失败！");
        }
    }
}
class ServerThread extends Thread{//创建子类线程
    Socket socket;//创建套接字
    DataOutputStream out=null;//创建数据输出流
    DataInputStream in=null;//创建数据输入流
    String sql;
    ServerThread(Socket t){
        socket=t;
        try {
            out=new DataOutputStream(socket.getOutputStream());//将数据输出流与套接字连接起来
            in=new DataInputStream(socket.getInputStream());//将数据输入流与套接字连接起来
        }
        catch (IOException ignored){}
    }
    public void run(){
        while (true){
            try {//获取连接的请求
                String s1=in.readUTF();
                if (s1.startsWith("账号:")){//检查字符串是否是以指定字符串开头
                    s1=s1.substring(s1.indexOf(":")+1);//返回字符串的子字符串
                    sql="select * from 用户信息表 where 账号="+s1;
                }
                else if (s1.startsWith("昵称:")){
                    s1=s1.substring(s1.indexOf(":")+1);//返回字符串的子字符串
                    sql="select * from 用户信息表 where 昵称="+s1;
                }

            }
            catch (IOException e){}
        }
    }
}