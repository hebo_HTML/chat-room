package Sever;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class GetDBConnection {//连接数据库代码
    public static Connection connectDB(String DBName,String id,String p){
        Connection con=null;
        String
                url="jdbc:mysql://localhost:3306/"+DBName+"?" +
                "useSSL=true&characterEncoding=utf-8";
        try {
            Class.forName("com.mysql.jdbc.Driver");//加载JDBC-MySQL驱动
        }
        catch (Exception ignored){}
        try {
            con= DriverManager.getConnection(url,id,p);//连接代码
        }
        catch (SQLException ignored){}
        return con;
    }
}
